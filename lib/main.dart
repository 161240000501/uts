import 'package:flutter/material.dart';
import './product_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UTS',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Samsul Maarif, 161240000501'),
        ),
        body: ProductManager(),
      ),
    );
  }
}